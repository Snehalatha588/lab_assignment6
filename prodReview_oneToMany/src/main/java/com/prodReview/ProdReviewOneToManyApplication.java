package com.prodReview;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.prodReview.entities.Product;
import com.prodReview.entities.Review;
import com.prodReview.repo.ProductRepo;
import com.prodReview.repo.ReviewRepo;

@SpringBootApplication
public class ProdReviewOneToManyApplication implements CommandLineRunner {

	@Autowired
	private ProductRepo productRepo;
	@Autowired
	private ReviewRepo reviewRepo;

	public static void main(String[] args) {
		SpringApplication.run(ProdReviewOneToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Product prod1 = new Product("Laptop");
		Product prod2 = new Product("Mobile");

		Review rev1 = new Review("good", prod1);
		Review rev2 = new Review("average", prod1);
		Review rev3 = new Review("excellent", prod2);
		Review rev4 = new Review("good", prod2);

		productRepo.save(prod1);
		productRepo.save(prod2);

		reviewRepo.save(rev1);
		reviewRepo.save(rev2);
		reviewRepo.save(rev3);
		reviewRepo.save(rev4);

	}

}
