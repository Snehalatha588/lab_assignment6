package com.prodReview.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reviewDetails")
public class Review {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int rid;
	private String comments;
	@JoinColumn(name = "prodId_fk")
	@ManyToOne
	private Product product;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Review() {
	}

	public Review(String comments, Product product) {
		super();
		this.comments = comments;
		this.product = product;
	}

	@Override
	public String toString() {
		return "Review [rid=" + rid + ", comments=" + comments + ", product=" + product + "]";
	}

}
