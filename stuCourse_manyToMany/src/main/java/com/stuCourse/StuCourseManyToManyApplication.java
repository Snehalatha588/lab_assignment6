package com.stuCourse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.stuCourse.entities.Course;
import com.stuCourse.entities.Student;
import com.stuCourse.repo.CourseRepo;
import com.stuCourse.repo.StudentRepo;

@SpringBootApplication
public class StuCourseManyToManyApplication implements CommandLineRunner {

	@Autowired
	private StudentRepo studentRepo;
	@Autowired
	private CourseRepo courseRepo;

	public static void main(String[] args) {
		SpringApplication.run(StuCourseManyToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Course c1 = new Course("C");
		Course c2 = new Course("JAVA");
		Course c3 = new Course("ANGULAR");

		Student s1 = new Student("venkatlaxmi");
		Student s2 = new Student("sneha");
		Student s3 = new Student("jyotshna");

		s1.getCourses().add(c1);
		s1.getCourses().add(c2);
		s2.getCourses().add(c2);
		s3.getCourses().add(c3);

		c1.getStudents().add(s1);
		c2.getStudents().add(s1);
		c2.getStudents().add(s2);
		c3.getStudents().add(s3);

		studentRepo.save(s1);
		studentRepo.save(s2);
		studentRepo.save(s3);

		courseRepo.save(c1);
		courseRepo.save(c2);
		courseRepo.save(c3);

	}

}
