package com.stuCourse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stuCourse.entities.Course;

@Repository
public interface CourseRepo extends JpaRepository<Course, Integer> {

}
