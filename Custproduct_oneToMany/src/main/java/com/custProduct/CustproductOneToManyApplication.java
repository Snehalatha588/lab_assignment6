package com.custProduct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.custProduct.entities.Customer;
import com.custProduct.entities.Product;
import com.custProduct.repo.CustomerRepo;
import com.custProduct.repo.ProductRepo;

@SpringBootApplication
public class CustproductOneToManyApplication implements CommandLineRunner {
	@Autowired
	private CustomerRepo custRepo;
	@Autowired
	private ProductRepo prodRepo;

	public static void main(String[] args) {
		SpringApplication.run(CustproductOneToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Customer cust1 = new Customer("venkatlaxmi");
		Customer cust2 = new Customer("jyotshna");

		Product prod1 = new Product("Mobile", cust1);
		Product prod2 = new Product("Laptop", cust1);
		Product prod3 = new Product("Television", cust2);
		Product prod4 = new Product("Refrigerator", cust2);

		custRepo.save(cust1);
		custRepo.save(cust2);

		prodRepo.save(prod1);
		prodRepo.save(prod2);
		prodRepo.save(prod3);
		prodRepo.save(prod4);

	}

}
