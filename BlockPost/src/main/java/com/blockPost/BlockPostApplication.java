package com.blockPost;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.blockPost.entities.Location;
import com.blockPost.entities.Posts;
import com.blockPost.entities.Users;
import com.blockPost.repo.LocationRepo;
import com.blockPost.repo.PostsRepo;
import com.blockPost.repo.UsersRepo;

@SpringBootApplication
public class BlockPostApplication implements CommandLineRunner {

	@Autowired
	private UsersRepo userRepo;
	@Autowired
	private PostsRepo postRepo;
	@Autowired
	private LocationRepo locationRepo;

	public static void main(String[] args) {
		SpringApplication.run(BlockPostApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Location loc1 = new Location("Srikakulam");
		Location loc2 = new Location("Visakhapatnam");

		Users user1 = new Users("VenkatLaxmi", "Ladi", loc1, "venkatlaxmi@gmail.com");
		Users user2 = new Users("Jyotshna", "Ladi", loc1, "jyotshna@gmail.com");
		Users user3 = new Users("priya", "salana", loc2, "priya.s@gmail.com");
		Users user4 = new Users("jagadamba", "kenguva", loc2, "jagadamba@gmail.com");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate d1 = LocalDate.parse("12/12/2019", formatter);
		LocalDate d2 = LocalDate.parse("10/08/2016", formatter);
		LocalDate d3 = LocalDate.parse("04/06/2020", formatter);
		LocalDate d4 = LocalDate.parse("26/09/2018", formatter);
		LocalDate d5 = LocalDate.parse("20/09/2017", formatter);
		LocalDate d6 = LocalDate.parse("25/09/2019", formatter);
		Posts post1 = new Posts(d1, user1, "abc");
		Posts post2 = new Posts(d2, user1, "xyz");
		Posts post3 = new Posts(d3, user2, "dfk");
		Posts post4 = new Posts(d4, user3, "pqr");
		Posts post5 = new Posts(d5, user3, "ajd");
		Posts post6 = new Posts(d6, user4, "dhf");

		locationRepo.save(loc1);
		locationRepo.save(loc2);

		userRepo.save(user1);
		userRepo.save(user2);
		userRepo.save(user3);
		userRepo.save(user4);

		postRepo.save(post1);
		postRepo.save(post2);
		postRepo.save(post3);
		postRepo.save(post4);
		postRepo.save(post5);
		postRepo.save(post6);
	}

}
